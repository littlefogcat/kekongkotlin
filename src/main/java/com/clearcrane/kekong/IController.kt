package com.clearcrane.kekong

/**
 * Created by jjy on 2018/2/2.
 */
interface IController {
    /**
     * 通过设备名称控制设备。名称符合的设备将会被控制。
     *
     * @param name 设备名称
     * @param cmd 控制指令
     */
    fun controlByName(name: String, cmd: String)


    /**
     * 通过设备关键词控制设备。名称中包含该关键词的设备都会被控制。
     *
     * @param keyword 设备关键词
     * @param cmd 控制指令
     */
    fun controlKeyword(keyword: String, cmd: String)

    /**
     * 通过设备类别来进行控制。该类别下的设备会被控制。
     *
     * @param type 设备类别
     * @param cmd 控制指令
     */
    fun controlByType(type: String, cmd: String)
}