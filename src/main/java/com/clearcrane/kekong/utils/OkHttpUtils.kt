package com.clearcrane.kekong.utils

import okhttp3.MediaType
import okhttp3.RequestBody

/**
 * Created by jjy on 2018/2/2.
 */
object OkHttpUtils {
    fun getJsonBody(json: String?): RequestBody {
        val jsonStr = json ?: "{}"
        return RequestBody.create(MediaType.parse("json/application"), jsonStr)
    }
}