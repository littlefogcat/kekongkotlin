package com.clearcrane.kekong.main

import android.view.View
import com.clearcrane.kekong.utils.OkHttpUtils
import okhttp3.*
import java.io.IOException

/**
 * Created by jjy on 2018/2/2.
 *
 * 负责网络请求
 */
class NetworkManager {
    private val mClient: OkHttpClient = OkHttpClient()

    private fun send(method: String,
                     url: String,
                     jsonParam: String?,
                     headers: Headers?,
                     listener: NetworkListener) {
        val requestBuilder: Request.Builder = Request.Builder()
        requestBuilder.url(url)
        when (method) {
            "get" -> requestBuilder.get()
            "post" -> requestBuilder.post(OkHttpUtils.getJsonBody(jsonParam))
        }
        if (headers != null) requestBuilder.headers(headers)
        val request: Request = requestBuilder.build()

        mClient.newCall(request)
                .enqueue(object : Callback {
                    override fun onResponse(call: Call?, response: Response?) {
                        listener.onSuccess(response?.body()?.string())
                    }

                    override fun onFailure(call: Call?, e: IOException?) {
                        e?.let { listener.onFailed(it) }
                    }
                })
    }

    fun get(url: String,
            jsonParam: String?,
            headers: Headers?,
            listener: NetworkListener) {
        send("get", url, jsonParam, headers, listener)
    }

    fun post(url: String,
             jsonParam: String?,
             headers: Headers?,
             listener: NetworkListener) {
        send("post", url, jsonParam, headers, listener)
    }
}