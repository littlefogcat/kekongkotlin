package com.clearcrane.kekong.main

import android.app.Activity
import android.os.Bundle
import com.clearcrane.kekongkotlin.R

class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
