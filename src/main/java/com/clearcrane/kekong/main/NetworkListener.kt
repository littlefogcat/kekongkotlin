package com.clearcrane.kekong.main

/**
 * Created by jjy on 2018/2/2.
 */
interface NetworkListener {
    fun onSuccess(response: String?)

    fun onFailed(e: Throwable) {}
}