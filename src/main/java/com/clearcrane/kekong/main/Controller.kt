package com.clearcrane.kekong.main

import android.content.Context
import com.clearcrane.kekong.IController

/**
 * Created by jjy on 2018/2/1.
 *
 * 控制器
 */
class Controller(context: Context) : IController {
    companion object {
        val API_AUTH = "http://iotdtest.cleartv.cn/iotd_backend/getRoomToken"
        val API_DEVICE = "http://iotdtest.cleartv.cn/iotd_backend/devList?roomNo=clear003"
        val API_CONTROL = "http://iotdtest.cleartv.cn/iotd_backend/devControl"
        val API_SCENE_CTRL = "http://iotdtest.cleartv.cn/iotd_backend/sceneControl"
    }

    private val mNetworkManager = NetworkManager()

    override fun controlByType(type: String, cmd: String) {
    }

    override fun controlKeyword(keyword: String, cmd: String) {
    }

    override fun controlByName(name: String, cmd: String) {
    }
}